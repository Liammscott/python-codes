#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 17:19:49 2018

@author: scottl
"""
import math
xs=input('input a value of x: ')
x=int(xs)
ys=input('input a value of y: ')
y=int(ys)
print('x^y=', x**y)
xl=math.log2(x)
print('log2(x)=', xl)