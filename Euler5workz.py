#Smallest number divisible by all numbers 1-20
#We only need to check the primes
#We only need to check the primes up to root(num)
"""This needs to be debugged in a big way"""
import numpy as np
import math
prod=1

#Method: we will find the prime factors of each number 1-20.
#We will append each of these prime numbers to a list called prime_factors
#The product of this list will be the smallest possible number divisible by all integers 1-20
#This is the case as the product found is divisible by all prime factors (which correspond to the prime factor form of each number 1-20)

def i_is_a_factor_of_x(i,x):
    if x%i == 0 and i!=1:
        return True


def factorise_x_by_i(x,i):
    x = int(x/i)
    return x


def find_factors_of(x):
    """This function finds the prime factors of x, returns them as a list """
    factors=[]
    i=1 #i is a count
    while i<=x and x!=1:
        if i_is_a_factor_of_x(i,x):
            factors.append(i)
            x = factorise_x_by_i(x,i)
            #reset i to 1 (this is so, e.g. if we factorise 10, we get to i=2, factorise 10 to 5, and then we factorise 5 starting at i=1 not i=2)
            i=1
        else:
            #if i is not a factor of x (or i =1) simply iterate to the next integer we want to test
            i+=1
    return factors

#make a list of prime factors, this is the list we will find the product of
prime_factors=[]
#we want to find all prime factors of all numbers 1-20
for i in range(1,21,1):
#we want to look at each number k in each set of factors for each i
    for k in range(0,len(find_factors_of(i)),1):
    #check if the kth factor of i has been included in the prime factors list alreadyself.
    #for example 2 has prime factor 2, 4 has prime factors (2,2), we only want to include the 2nd 2 as we already have a factor of 2 in the list from factorising 2.
        if  prime_factors.count(find_factors_of(i)[k])<find_factors_of(i).count(find_factors_of(i)[k]):
            #print('appending factor:', find_factors_of(i)[k])
            prime_factors.append(find_factors_of(i)[k])


#Now we find the product of prime factors
prod=1

for k in range(0,len(prime_factors),1):
    prod=prod*prime_factors[k]

print('prod=', prod)
for i in range(1,21):
    if prod%i==0:
        print(prod, ' is divisible by', i)
