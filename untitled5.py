#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 17:29:08 2018

@author: scottl
"""
sumT=0
def isdiv3(number):
    if number%3==0:
        return True
    else:
        return False

def isdiv5(number):
     if number%5==0:
        return True
     else:
        return False

def isdiv53(number):
    if isdiv3(number) and isdiv5(number):
        return True
    else:
        return False
    
for i in range(1001):
    if isdiv53(i):
        sumT+=i
    elif isdiv5(i):
        sumT+=i
    elif isdiv3(i):
        sumT+=i

print('total is=', sumT)
        