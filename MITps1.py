#Calculates what your savings are at the end of each month when the monthly rate is applied
def savings_after_monthly_returns(savings,returns_annual):
    returns_monthly=returns_annual/12
    savings=(1+returns_monthly)*savings
    return savings
#Increases your salary by your semi annual rate every 6 months
def salary_raise(current_salary, raise_dec):
    return (current_salary*(1+raise_dec))
#Calculates your savings after 36 months with a given savings_rate
def savings_at_36months(savings_rate):
    #initial parameters
    current_savings=0
    annual_salary= 150000
    monthly_salary=(annual_salary/12)
    semi_annual_raise=0.07
    r=0.04
    #iterating across 36 months and finding your savings
    for i in range(36):
        if i%6==0 and i!=0:
            annual_salary=salary_raise(annual_salary,semi_annual_raise)
            print('annual_salary:', annual_salary)
        current_savings=savings_after_monthly_returns(current_savings,r)
        current_savings=current_savings+(annual_salary/12)*savings_rate

        i+=1
    return current_savings
#Use bisection to find optimum savings rate
def bisect_method_savings(savings_target,error): #x is target we want to save, y is error on the value we find
    print('bisect method init') #check the function has been called
    count=1 #initialise the count
    portion_saved_lower=0 #set initial lower bound 0
    portion_saved_upper=10000 # set initial upper band 10000
    portion_saved_num=int((portion_saved_upper+portion_saved_lower)*0.5)#average the upper and lower band
    #savings_at_36months(portion_saved_num/10000)
    while abs(savings_at_36months(portion_saved_num/10000)-savings_target)>=error: #check if we are within the error margin of the number we want
         #iterate
        if savings_at_36months(portion_saved_num/10000)<savings_target:
            portion_saved_lower=portion_saved_num #change lower limit if estimate too low
        else:
            portion_saved_upper=portion_saved_num   #change upper limit if estimate too high
        portion_saved_num=int((portion_saved_upper+portion_saved_lower)/2.0) #try a new guess within the new limits
        count+=1
    print('£', savings_at_36months(portion_saved_num/10000),' saved using a rate of', portion_saved_num/10000)
    return count


"""Finding the best savings rate to save a given amount of money in a given time"""
portion_down_payment=0.25
total_cost=1000000
target_to_save=(portion_down_payment)*(total_cost)
print(savings_at_36months(0.3))
bisect_method_savings(target_to_save,10)

"""Finding how long it takes to save a given amount of money """

"""Variables input here"""
current_savings=0
annual_salary=float(input('input your annual salary in £: '))
savings_rate=float(input('input the portion of your salary you want to save as a decimal: '))
total_cost=float(input('input the total cost of the house you\'re saving for:'))
semi_annual_raise=float(input('input your semi annual raise as a decimal portion of your annual salary: '))
portion_down_payment=float(input('input the fraction of the house price you need to pay as a downpayment: '))
r=0.04
target_to_save=portion_down_payment*total_cost #the amount you need to save for the downpayment
month=1 #start during month 1
"""This loop finds your savings each month stopping when you have reached your target """
while current_savings<=target_to_save:
    if month%6==0:
        annual_salary=salary_raise(annual_salary,semi_annual_raise)
    current_savings=current_savings+(annual_salary/12)*savings_rate
    current_savings=savings_after_monthly_returns(current_savings,r)
    month+=1
print('saved £',current_savings, ' in ', month, ' months')
