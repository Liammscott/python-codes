#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  1 15:24:47 2018

@author: scottl
"""

def iseven(number):
        if number%2==0:
          return True
        if number%2!=0:
          return False

def fib(n):
    if n==0 or n==1:
        return 1
    else:
        return fib(n-1)+fib(n-2)

def testfib(n):
    for i in range(n+1):
        print('fib of', i, '=', fib(i))
        if fib(n) == fib(2):
            print("FIB 2")
testfib(89)