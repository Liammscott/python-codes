import unittest
import hangman as hgmn

class testfunc(unittest.TestCase):
    def setUp(self):
        print("setting up")
    def tearDown(setUp):
        print()
    def test1(self):
        #This function checks if the word is guessed
        print("running True test")
        secret_word='hang'
        letters_guessed='hang'
        self.assertTrue(hgmn.is_word_guessed(secret_word, letters_guessed))

    def test1a(self):
        #This function checks if the word is guesse,repeated letters seem to be an issue sometimes
        print("running True test")
        secret_word='apple'
        letters_guessed='apple'
        self.assertTrue(hgmn.is_word_guessed(secret_word, letters_guessed))

    def test1b(self):
        #This function checks if the word is guessed,repeated letters seem to be an issue sometimes
        #Can check function gives proper response for right letters wrong order
        print("running True test")
        secret_word='apple'
        letters_guessed='apple'
        self.assertTrue(hgmn.is_word_guessed(secret_word, letters_guessed))

    def test1b(self):
        #This function checks if the word is guessed,repeated letters seem to be an issue sometimes
        #Can check function gives proper response for right letters but arbitrary order
        print("running True test")
        secret_word='apple'
        letters_guessed='lpape'
        self.assertTrue(hgmn.is_word_guessed(secret_word, letters_guessed))


    def test2(self):
        print("running False test")
        secret_word='hang'
        letters_guessed='hand'
        self.assertFalse(hgmn.is_word_guessed(secret_word, letters_guessed))
    def test3(self):
        print("get guessed word test")
        secret_word='hang'
        letters_guessed='hn'
        self.assertEqual('h_n_',hgmn.get_guessed_word(secret_word, letters_guessed))

    def test3(self):
        #should try with double letters again
        print("get guessed word test")
        secret_word='apple'
        letters_guessed='ep'
        self.assertEqual('_pp_e',hgmn.get_guessed_word(secret_word, letters_guessed))

    def test4(self):
        print("get available letters")
        letters_guessed=['m','n','o']
        self.assertEqual('abcdefghijklpqrstuvwxyz', hgmn.get_available_letters(letters_guessed))
    def test5(self):
        print("match with gaps test, match")
        match_word='tatt'
        secret_word='t__t'
        self.assertTrue(hgmn.match_with_gaps(secret_word, match_word))
    def test6(self):
        print("match with gaps test, match word too long")
        match_word='tattt'
        secret_word='t__t'
        self.assertFalse(hgmn.match_with_gaps(secret_word, match_word))
    def test7(self):
        print("match with gaps test, match word too short")
        match_word='tat'
        secret_word='t__t'
        self.assertFalse(hgmn.match_with_gaps(secret_word, match_word))
    def test8(self):
        print("match with gaps test, match word wrong letter")
        match_word='baht'
        secret_word='t__t'
        self.assertFalse(hgmn.match_with_gaps(secret_word, match_word))

    # def test9(self):
    #     hgmn.show_possible_matches('b___')

if __name__=='__main__':
    unittest.main() #invokes runner on the extended class
