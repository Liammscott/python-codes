#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 28 17:04:26 2018

@author: scottl
"""
##find x such that x**2-24 is within epsilon of 0
epsilon=0.01
k=999999999999999999999999
guess=k/2.0
countNR=1
while abs(guess*guess-k)>=epsilon:
    #print('guess number: ', countNR)
    guess=guess-(((guess**2)-k)/(2*guess))
    countNR+=1
print('square root of', k,'is about', guess)
print('This took: ', countNR, ' guesses')

countBi=1
low=0
high=max(1.0,k)
ans=(high+low)/2.0
while abs(ans**2-k)>=epsilon:
    #print('low= ', low, 'high= ', high, 'guess= ', ans)
    countBi+=1
    if ans**2<k:
        low=ans
    else:
        high=ans
    ans=(high+low)/2.0

print(ans, 'is close to the square root of', k)
print('This took: ', countBi, ' guesses' )