# Problem Set 2, hangman.py
# Name:
# Collaborators:
# Time spent:

# Hangman Game
# -----------------------------------
# Helper code
# You don't need to understand this helper code,
# but you will have to know how to use the functions
# (so be sure to read the docstrings!)
import random
import string

WORDLIST_FILENAME = "words.txt"

alphabet=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']

def load_words():
    """
    Returns a list of valid words. Words are strings of lowercase letters.

    Depending on the size of the word list, this function may
    take a while to finish.
    """
    print("Loading word list from file...")
    # inFile: file
    inFile = open(WORDLIST_FILENAME, 'r')
    # line: string
    line = inFile.readline()
    # wordlist: list of strings
    wordlist = line.split()
    print("  ", len(wordlist), "words loaded.")
    return wordlist



def choose_word(wordlist):
    """
    wordlist (list): list of words (strings)

    Returns a word from wordlist at random
    """
    return random.choice(wordlist)

# end of helper code

# -----------------------------------

# Load the list of words into the variable wordlist
# so that it can be accessed from anywhere in the program
wordlist = load_words()

def is_word_guessed(secret_word, letters_guessed):
    letters_are_in = 0
    letters_in_secret_word = {}
    unique_guesses={}
    unique_letters_in_secret_word={}

    for i in range(len(secret_word)):
        letters_in_secret_word.update({secret_word[i]:i})
    count=0
    for l in letters_in_secret_word:
        if l not in unique_letters_in_secret_word:
            unique_letters_in_secret_word.update({l:count})
            count+=1
    # print('unique letters in secret_word: ', unique_letters_in_secret_word)
    for j in range(len(letters_guessed)):

        if letters_guessed[j] not in unique_guesses:
            unique_guesses.update({letters_guessed[j]:j})
            # print('unique guesses:', unique_guesses)
    for k in unique_letters_in_secret_word:
        if k in unique_guesses:

            letters_are_in+=1
            # print('letter ', k, 'is in guessed word')


    if letters_are_in == len(unique_letters_in_secret_word):
        # print('letters guessed in the word: ', letters_are_in, 'length of the word:', len(secret_word))
        # print('word is guessed')
        return True
    if letters_are_in!=len(unique_letters_in_secret_word):
        # print('word is not guessed :(')
        # print('letters guessed in the word: ', letters_are_in, 'length of the word:', len(secret_word))
        return False
    '''
    secret_word: string, the word the user is guessing; assumes all letters are
      lowercase
    letters_guessed: list (of letters), which letters have been guessed so far;
      assumes that all letters are lowercase
    returns: boolean, True if all the letters of secret_word are in letters_guessed;
      False otherwise
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"



def get_guessed_word(secret_word, letters_guessed):
    letters_guessed_dict = {}
    guessed_word = str()
    for i in range(len(letters_guessed)):
        letters_guessed_dict.update({letters_guessed[i]:i})
    for j in secret_word:
        if j in letters_guessed_dict:
            guessed_word+=j
        else:
            guessed_word+='_'
    return guessed_word
    '''
    secret_word: string, the word the user is guessing
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string, comprised of letters, underscores (_), and spaces that represents
      which letters in secret_word have been guessed so far.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"




def get_available_letters(letters_guessed):
    letters_guessed_dict={}
    available_letters=str()
    for i in range(len(letters_guessed)):
        letters_guessed_dict.update({letters_guessed[i]:i})
    for j in range(len(alphabet)):
        if alphabet[j] not in letters_guessed_dict:
            available_letters+=alphabet[j]

    return available_letters


    '''
    letters_guessed: list (of letters), which letters have been guessed so far
    returns: string (of letters), comprised of letters that represents which letters have not
      yet been guessed.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"




def hangman(secret_word):
    length_of_secret_word = int(len(secret_word))
    secret_word_dict = {}
    for i in range(length_of_secret_word):
        secret_word_dict.update({secret_word[i]:i})
    guesses=6
    guessed_letters=[]

    print("let's play hangman!")
    round = 1


    # while guesses != 0 and is_word_guessed(secret_word, guessed_letters) == False:
    while guesses!=0 and is_word_guessed(secret_word, guessed_letters) == False:

        print("\n \n \n ******************** \n round: ", round)
        print(" You have", guesses, " guesses to find the ", length_of_secret_word, " letter secret word!")
        print("Letters not guessed: ", get_available_letters(guessed_letters))
        print(get_guessed_word(secret_word, guessed_letters))


        user_guess_input = str(input("Input your guess: "))

        if user_guess_input in guessed_letters:
            guesses-=1
        elif user_guess_input not in secret_word_dict:
            guesses-=1

        guessed_letters.append(user_guess_input)


    if is_word_guessed(secret_word, guessed_letters) == True:
        print('You won!')
    if guesses == 0:
        print('You lost!')



    '''
    secret_word: string, the secret word to guess.

    Starts up an interactive game of Hangman.

    * At the start of the game, let the user know how many
      letters the secret_word contains and how many guesses s/he starts with.

    * The user should start with 6 guesses

    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.

    * Ask the user to supply one guess per round. Remember to make
      sure that the user puts in a letter!

    * The user should receive feedback immediately after each guess
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the
      partially guessed word so far.

    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"




# When you've completed your hangman function, scroll down to the bottom
# of the file and uncomment the first two lines to test
#(hint: you might want to pick your own
# secret_word while you're doing your own testing)


# -----------------------------------


def match_with_gaps(my_word, other_word):
    if len(my_word)==len(other_word):
        letters_right=0
        letters_in_my_word=[]
        for letter in range(len(my_word)):
            if my_word[letter]!='_':
                # print('letter in my word:', my_word[letter])
                letters_in_my_word.append(my_word[letter])
                if my_word[letter]==other_word[letter]:
                    # print('letter in other word:', other_word[letter])
                    letters_right+=1
        # print('there are:', len(letters_in_my_word), 'letters given in my_word')
        if letters_right==len(letters_in_my_word):
            # print('match')
            return True
        else:
            # print('no match, wrong letters')
            return False
    else:
        # print('no match, wrong length')
        return False


    '''
    my_word: string with _ characters, current guess of secret word
    other_word: string, regular English word
    returns: boolean, True if all the actual letters of my_word match the
        corresponding letters of other_word, or the letter is the special symbol
        _ , and my_word and other_word are of the same length;
        False otherwise:
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"




def show_possible_matches(my_word):
    for i in wordlist:
        # print(i)
        if match_with_gaps(my_word,i):

            print(i)

    '''
    my_word: string with _ characters, current guess of secret word
    returns: nothing, but should print out every word in wordlist that matches my_word
             Keep in mind that in hangman when a letter is guessed, all the positions
             at which that letter occurs in the secret word are revealed.
             Therefore, the hidden letter(_ ) cannot be one of the letters in the word
             that has already been revealed.

    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"




def hangman_with_hints(secret_word):

        length_of_secret_word = int(len(secret_word))
        secret_word_dict = {}
        for i in range(length_of_secret_word):
            secret_word_dict.update({secret_word[i]:i})
        guesses=6
        guessed_letters=[]

        print("let's play hangman!")
        round = 1


        # while guesses != 0 and is_word_guessed(secret_word, guessed_letters) == False:
        while guesses!=0 and is_word_guessed(secret_word, guessed_letters) == False:

            print('\n \n \n ******************** \n round: ', round)
            print(" You have", guesses, " guesses to find the ", length_of_secret_word, " letter secret word!")
            print("Letters not guessed: ", get_available_letters(guessed_letters))
            print(get_guessed_word(secret_word, guessed_letters))


            user_guess_input = str(input("Input your guess: "))
            if user_guess_input == '*':
                show_possible_matches(get_guessed_word(secret_word, guessed_letters))

            elif user_guess_input in guessed_letters and user_guess_input !='*':
                guesses-=1
            elif user_guess_input not in secret_word_dict:
                guesses-=1

            guessed_letters.append(user_guess_input)


        if is_word_guessed(secret_word, guessed_letters) == True:
            print('You won!')
        if guesses == 0:
            print('You lost!')
        '''
    secret_word: string, the secret word to guess.

    Starts up an interactive game of Hangman.

    * At the start of the game, let the user know how many
      letters the secret_word contains and how many guesses s/he starts with.

    * The user should start with 6 guesses

    * Before each round, you should display to the user how many guesses
      s/he has left and the letters that the user has not yet guessed.

    * Ask the user to supply one guess per round. Make sure to check that the user guesses a letter

    * The user should receive feedback immediately after each guess
      about whether their guess appears in the computer's word.

    * After each guess, you should display to the user the
      partially guessed word so far.

    * If the guess is the symbol *, print out all words in wordlist that
      matches the current guessed word.

    Follows the other limitations detailed in the problem write-up.
    '''
    # FILL IN YOUR CODE HERE AND DELETE "pass"




# When you've completed your hangman_with_hint function, comment the two similar
# lines above that were used to run the hangman function, and then uncomment
# these two lines and run this file to test!
# Hint: You might want to pick your own secret_word while you're testing.


if __name__ == "__main__":
    # pass

    # To test part 2, comment out the pass line above and
    # uncomment the following two lines.

    #secret_word = choose_word(wordlist)

    """comment out one of the games to play with or without hints"""
    #hangman(secret_word)
    #hangman_with_hints(secret_word)
    # hangman(secret_word)

###############

    # To test part 3 re-comment out the above lines and
    # uncomment the following two lines.

    #secret_word = choose_word(wordlist)
    #hangman_with_hints(secret_word)
