#Euler4
#This function checks if a number is palindromic
#Still have work to do on palindrom_rec function
#this is a recursive function for checking if number is palindrome
def ispalindrome_rec1(s):
    while len(s)!=1:
        if len(s)==2 and s[0]==s[-1]:
            return True
        if s[0]==s[-1]:
            s=s[1:-1]
            ispalindrome_rec1(s)
        if s[0]!=s[-1]:
            return False
    return True

#This is a function for iteratively checking if a number is a palindrome
def ispalindrome1(s):
     while len(s)!=1:
        if len(s)==2 and s[0]==s[-1]:
            return True
        if s[0]==s[-1]:
            s=s[1:-1]
        if s[0]!=s[-1]:
            return False
     return True






if ispalindrome_rec1('9991999')==True:
    print('Test1 success')
if ispalindrome_rec1('99911999')==True:
    print('Test2 success')
if ispalindrome_rec1('9991199')!=True:
    print('Test3 success')
if ispalindrome_rec1('919999')!=True:
    print('Test4 success')
if ispalindrome_rec1('919999')==False:
    print('Test5 success')
if ispalindrome_rec1('99911999')==False:
    print('Test6 fail')
if ispalindrome_rec1('99911999')!=True:
    print('Test7 fail')
biggest=0
count=0
for i in range(100,999,1):
    for j in range(100,999,1):
#This next line is super useful, it means we wont check repeat products, this will cut down processing time
        if j>i:
            k=i*j
            l=str(k)
            count+=1
            if ispalindrome_rec1(l)!=False:
                if k>biggest:
                    biggest=k
                    print('biggest palindromic number= ', biggest)
print('count= ', count)
print(biggest)
