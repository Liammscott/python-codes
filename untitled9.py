#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  1 16:45:14 2018

@author: scottl
"""




r=0.04
months=0
current_savings=0

total_cost=int(input('what is the total cost of the house'))
portion_saved=float(input('what percentage of your income will you save'))
annual_salary=int(input('what is your annual salary'))
semi_annual_raise=float(input('what is your semi annual raise'))

while current_savings<0.25*total_cost:
    if months%6==0:
        annual_salary+=annual_salary*(  semi_annual_raise)
    saving_increase_month=current_savings*r/12+annual_salary/120
    current_savings=current_savings+saving_increase_month
    print('current savings= ', current_savings, 'savings increase= ', saving_increase_month)
    months+=1

print('will take', months, ' months')